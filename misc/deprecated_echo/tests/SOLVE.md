# Deprecated Echo Solve

Decode using

```sh
./steglsb.py -d ../nft_lsb.jpg 3 ../outfile.png
```

in https://github.com/Aqcurate/lsb-steganography

View image, get link to https://pastebin.com/9bRN2Eah

Code is the code of the underlying echo service

Find vuln in service with use of `input()` in python2 (it's the same as `eval(input())` in python3).

Exploit that vuln any number of ways to get flag.
