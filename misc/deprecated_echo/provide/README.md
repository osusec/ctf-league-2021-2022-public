# Deprecated Echo

Top level software determined that this image is suspicious. We cannot find out why, so we ask you to find out what information it holds. We think it is the source code for an antiquated echo service which we have discovered here:

<chal link>

The program that detected that this image is suspicious also spit out the information:

> Bits=3

Flag is in flag.txt

# Tools

Some sort of LSB Stego tool, recommended: https://github.com/Aqcurate/lsb-steganography
