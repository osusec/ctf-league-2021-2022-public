#!/usr/bin/env python2

if __name__ == '__main__':

    try:
        # Get User Input and file  as Strings
        user_input = raw_input("Enter a string:")
        user_file = raw_input("Enter a location in the current directory:")

        # Check Directory
        if ['.', '/'] in user_file:
            raise ValueError("Bruh")

        # Write user input to a file
        if user_input.isalpha():
            f = open(user_file, 'w')
            f.write(user_input)
            f.close()

        # Return the input to the user
        print user_input

    # Catch-all for exceptions
    except Exception as exc:
        print "Whoops, something happened :("

        # Dump the exception to somewhere
        file_location = input("Where should I dump crash log to?:")

        # Check file path is valid
        if ['.', '/'] in file_location:
            f = open(file_location, 'w')
            f.write(str(exc))
            f.close()
