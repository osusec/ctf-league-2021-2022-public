# Filename: server.py
# Info: our production-ready secure-server
#       for our ground-breaking AI algorithms.

from sanic import Sanic
from sanic.request import Request
from sanic.response import HTTPResponse, text
from sanic.exceptions import ServerError

# A bunch of logging imports to try and reduce
# the amount of logs tenserflow spits out 
import asyncio
import logging
logging.getLogger('tensorflow').disabled = True
import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3' 

# neural network model import
from model import BigBadMLCheck

app = Sanic("ShallowNet")
app.config.DEBUG = False # Production 4 lyfe
app.config.load_environment_vars(prefix="SANIC_")
app_context = {str(k):str(v) for k,v in app.config.items()}

# TODO: add devcode
app.config.DEVCODE = None

with open('flag.txt', 'r') as flag_file:
  app.config.FLAG = flag_file.read()

# Server static files (sanic doesn't auto server index.html at "/")
app.static('/', './public/index.html', name='index')
app.static('/', './public/', name='html_stff')

# Test auth route
@app.post("/authtest")
def authTest(request: Request) -> HTTPResponse:
  # TODO: delete route, it's useless
  body = request.json
  if 'admin' in body:
    if hash(body['admin']) == hash(app.config.PASSCODE):
      return text('yeah boii')
  # only return dev logic if we're verified dev
  assert body['devcode'] == app.config.DEVCODE
  raise ServerError(f"{body['admin']} is incorrect", status_code=401,quiet=False,context=app_context)


# OPTIMIZATION
PRESETS = {k:'green' for k in range(5)} | {k:'red' for k in range(5,10)}

@app.post("/classify")
async def classify(request: Request) -> HTTPResponse:
  body = request.json
  # simply an innocent optimization, not a lie--we'd never do that!
  if 'option' in body:
    return text(PRESETS[int(body['option'])], 200)
  # ok, now the actual algorithm
  if 'admin' in body:
    if body['admin'] == app.config.PASSCODE:
      if 'pic' in body:
        loop = asyncio.get_event_loop()
        answer = await loop.run_in_executor(None, BigBadMLCheck, body['pic'])
        if answer == 'flag':
          return text(app.config.FLAG)
        return text(answer)
      return text('ERR: no body found.', 400)
  return text('no no no, you can\'t access this API route.', 400)


app.run(host='0.0.0.0', port=8000, access_log=False, fast=False)

