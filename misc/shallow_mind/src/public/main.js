'use strict';

const RESULT = document.querySelector('#result')
const SELECTED = document.querySelector('#selected')

const IMAGES = {
  0: {
    src:'https://images.pexels.com/photos/40896/larch-conifer-cone-branch-tree-40896.jpeg',
    name: 'Conifer Cone Branch Tree'
  },
  1: {
    src:'https://cdn.pixabay.com/photo/2021/08/19/12/37/hill-6557956_1280.jpg',
    name: 'Hill'
  },
  2: {
    src:'https://cdn.pixabay.com/photo/2020/04/18/22/14/nature-5061217_1280.jpg',
    name: 'Ferns and stuff'
  },
  3: {
    src:'https://cdn.pixabay.com/photo/2016/07/08/08/42/background-1503842_1280.png',
    name: 'Green lines'
  },
  4: {
    src:'https://cdn.pixabay.com/photo/2016/04/27/07/00/course-1356075_1280.jpg',
    name:'Green Gradient'
  },
  5: {
    src:'https://cdn.pixabay.com/photo/2017/10/26/19/45/red-2892235_1280.png',
    name:'Red christmas paper'
  },
  6: {
    src:'https://cdn.pixabay.com/photo/2017/11/01/18/17/christmas-2908901_1280.jpg',
    name:'More christmas paper'
  },
  7: {
    src:'https://cdn.pixabay.com/photo/2014/03/22/17/03/the-background-292729_1280.png',
    name:'Red waves and stuff'
  },
  8: {
    src:'https://cdn.pixabay.com/photo/2016/06/20/03/49/background-1468009_1280.png',
    name:'Red sun decoration'
  },
  9: {
    src:'https://cdn.pixabay.com/photo/2012/03/04/00/24/background-21912_1280.jpg',
    name:'Bricks'
  }
}



let selectedURL = 0
let selectedName = ''
let selectedIndex = 0

function imageSelector(url, name, index){
  return function () {
    selectedURL = url
    selectedName = name
    selectedIndex = index
    SELECTED.textContent = "Selected Image: " + selectedName
  }
}

window.onload = () => {
  for (let [index, {src: image, name}] of Object.entries(IMAGES)){
    const imageEl = document.createElement('img')
    imageEl.src = image
    const size = 100
    imageEl.height = size
    imageEl.width = size
    imageEl.name = name
    imageEl.style = "cursor:pointer;"
    imageEl.onclick = imageSelector(image, name, index)
    RESULT.appendChild(imageEl)
  }
}


function ClassifyImage(){
  fetch('/classify', {
    method: 'POST',
    body: JSON.stringify({option:selectedIndex})
  }).then(res => {
    return res.text().then(txt => 
      document.querySelector('#resultText').textContent = `Result From Classifier: ${txt}!`
    )
  })
}
