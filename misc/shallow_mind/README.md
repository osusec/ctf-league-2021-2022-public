# Shallow Mind

```
chal_id: 7?
description: We suspect that the Shallow Mind R&D Branch of Maldiant has been lying to investors.  Can you check to see if their image recognition program is really all that?
flag: osu{1_d0nt_kn0w_h0w_2_tr@1n_nu3r@l_n3t5}
points: 200
name: shallow_mind
download: the zip achieve under the download directory has a local copy people can run on their machine. 
category: misc/ai
release_time: 6:10pm
num_weeks: 1
```

A very very simple neural-net challenge where the student feeds a strange image to the neural net to trigger a false-positive and retrieve the flag.
 
The motivation for this challenge comes from this [paper](https://arxiv.org/abs/2006.08131).  It doesn't really follow the vulnerability shown in the paper though--but it does show that neural nets are a bit finicky.
 
Unfortunately I don't know a lot about setting up machine learning models, so... the model I trained using https://teachablemachine.withgoogle.com/ with a very small data-set (by machine-learning standards) is uhh, really easy to trick (it will sometimes think a purely green image is red... I don't even know why).
 
The only real challenge with tricking this neural net is to get past a simple plaintext admin password check via leaking the sanic config through a very canned 'deprecated' API route.  The challenge will probably take around 2 minutes--but hopefully it communicates the fickle nature of a poorly-trained neural net.
