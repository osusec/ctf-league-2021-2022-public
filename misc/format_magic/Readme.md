Name: `format_magic`

Description: 
```
Time to correct some errors!

Note: Some (very light) brute forcing will be necessary to solve this challenge.

Helpful links:
https://en.wikipedia.org/wiki/List_of_file_signatures
https://merricx.github.io/qrazybox/
https://www.thonky.com/qr-code-tutorial/
```

Flag: `osu{c0rrup7ion_m4g1c}`
