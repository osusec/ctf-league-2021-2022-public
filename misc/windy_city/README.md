Name: `windy_city`

Description: Can you give the name of the artist who painted the mural that now resides on the left side of this white building? The flag is in the format `osu{first_last}`

Flag: `osu{max_sansing}`

Attachment:
![image](windy_city.png)
