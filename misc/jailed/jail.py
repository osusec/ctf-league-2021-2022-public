#!/usr/bin/python3

# Source code written by ALLES CTF team for ALLES CTF 2020
# https://github.com/allesctf/2020/blob/master/challenges/pyjail/deploy/pyjail.py

# Modified for use at CTF League 2022 by Lyell Read

import pickle
import base64

global original


def make_secure():
    global original
    original = __builtins__.__dict__.copy()
    __builtins__.__dict__.clear()
    safe_builtins = [
        "help",
        "input",
        "any",
        "print",
        "all",
        "Exception",
        "exec",
        "eval",
        "isinstance",
        "str",
        "bytes",
    ]
    for func in safe_builtins:
        __builtins__.__dict__[func] = original[func]


def make_insecure():
    global original
    __builtins__.__dict__.clear()
    for func in original:
        __builtins__.__dict__[func] = original[func]


def memfrob_byte(byte):
    return byte ^ 0x42


def DATA():
    make_insecure()
    user_input = input("What data do you want to decode?\n")
    decoded = base64.urlsafe_b64decode(user_input)
    out = []
    for byte in decoded:
        out.append(memfrob_byte(byte))
    safe_data = bytes(out)
    print(pickle.loads(safe_data))
    make_secure()


print("You get a simple function named 'DATA()'. That's it.")

denied = [
    "import",
    "upper",
    "lower",
    "open",
    "exit",
    "compile",
    "chr",
    "__import__",
    "object",
    "assert",
    "__builtins__",
    "exec",
    "pper",
    "per",
]
charset = [
    '"',
    "(",
    ")",
    "+",
    ".",
    "0",
    "1",
    "2",
    "[",
    "]",
    "_",
    "a",
    "e",
    "i",
    "n",
    "p",
    "t",
    "r",
    "u",
    "x",
    "v",
    "l",
]
make_secure()
while True:
    text = input(">>> a = ").lower()
    for char in text:
        if char not in charset:
            print(char)
    if any(word in text for word in denied) or not all(
        char in charset for char in text
    ):
        print("Denied")
    else:
        try:
            exec("a=" + text)
        except Exception as error:
            print(error)
