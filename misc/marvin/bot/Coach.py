from discord.ext import commands
from config import ANNOUNCEMENTS_CHANNEL_ID, BOT_ROLE_ID, GUILD_ID
import discord

class Coach(commands.Cog):
    def __init__(self, bot, db, log, sk):
        self.bot = bot
        self.db = db
        self.log = log
        self.sk = sk

    @commands.command(name='chal_create', help='sets up a new challenge')
    @commands.has_role('coach')
    async def chal_create(self, ctx, name, flag, points, num_weeks):
        chal_id = self.db.sql_commit("INSERT INTO challenges (name, flag, points, num_weeks) VALUES (%s, %s, %s, %s)", val=(name, flag, int(points), int(num_weeks)))
        await ctx.send('challenge created, id ' + str(chal_id))

    @commands.command(name='view_writeup', help='views the last submitted writeup')
    @commands.has_role('coach')
    async def view_writeup(self, ctx):
        writeup = self.db.sql_fetchone("SELECT text,writeup_id FROM writeups WHERE approved IS NULL ORDER BY writeup_id limit 1")
        await ctx.send(repr(writeup))

    @commands.command(name='approve_writeup', help='approves the specified writeup')
    @commands.has_role('coach')
    async def approve_writeup(self, ctx, writeup_id):
        challenge, user = self.db.sql_fetchone("SELECT chal_id,user_id FROM writeups WHERE writeup_id=%s", val=(writeup_id,))
        if self.db.sql_fetchone("SELECT writeup_id FROM writeups WHERE chal_id=%s AND user_id=%s AND approved=1", val=(challenge,user)):
            self.db.sql_commit("UPDATE writeups SET approved=0 WHERE writeup_id=%s", val=(writeup_id,))
            await ctx.send("already approved a writeup for this user and challenge, rejecting this one")
            return
        self.db.sql_commit("UPDATE writeups SET approved=1 WHERE writeup_id=%s", val=(writeup_id,))
        await ctx.send("approved")

    @commands.command(name='reject_writeup', help='rejects the specified writeup')
    @commands.has_role('coach')
    async def reject_writeup(self, ctx, writeup_id):
        self.db.sql_commit("UPDATE writeups SET approved=0 WHERE writeup_id=%s", val=(writeup_id,))
        await ctx.send("rejected")

    @commands.command(name='add_to_team', help='adds the specified player to the specified team')
    @commands.has_role('coach')
    async def add_to_team(self, ctx, player_id, team_id):
        # add player to users
        user = ctx.guild.get_member(int(player_id))
        self.db.sql_commit("INSERT INTO users (discord_id, nickname) VALUES (%s, %s) ON DUPLICATE KEY UPDATE nickname=%s", val=(user.id, user.name.encode('unicode-escape').decode()[:19], user.name.encode('unicode-escape').decode()[:19]))

        # figure out which is the next available player id
        players = self.db.sql_fetchone("SELECT " + ",".join(["player_%d" % i for i in range(21)]) + " FROM teams WHERE team_id=%s", val=(team_id,))
        new_player_id = 0
        for i in range(len(players)):
            if players[i] == None:
                new_player_id = i
                break

        # Add player to db
        self.db.sql_commit("UPDATE teams SET player_" + str(new_player_id) + "=%s WHERE team_id=%s", val=(player_id, team_id))

        # Add player role
        role = discord.utils.get(ctx.guild.roles, name=team_id)
        await ctx.guild.get_member(int(player_id)).add_roles(role)
        await ctx.send("success")

    @commands.command(name='scoreboard', help='updates the scoreboard')
    @commands.has_role('coach')
    async def scoreboard(self, ctx):
        guild = list(filter(lambda g: g.id == GUILD_ID, self.bot.guilds))[0]
        await self.sk.scoreboard(guild)
        await ctx.send('scoreboard updated')
