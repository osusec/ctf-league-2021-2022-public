// Coffee API routes
package com.corporate.coffeeCorporation;

import java.util.concurrent.atomic.AtomicLong;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.ui.Model;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.*;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;


@RestController
public class CoffeeController {

  private static final Logger logger = LogManager.getLogger("OrderLog");
  private final AtomicLong orderID = new AtomicLong();

  @GetMapping("/available")
  public ArrayList<Coffee> available(){
    int size = 3;
    ArrayList<Coffee> inventory = new ArrayList<Coffee>(size);
    inventory.add(new Coffee(0, 23, "Light Roast"));
    inventory.add(new Coffee(1, 49, "Medium Roast"));
    inventory.add(new Coffee(2, 94, "rgb(0,0,0) Roast"));
    return inventory;
  }

  @PostMapping("/order")
  public String orderUp(@ModelAttribute OrderForm order, Model model){
    String comments = order.getComments();

    String response;

    // ok fixed log4shell vuln, app is now secure.
    if (!(comments.contains("jndi"))){
      ByteArrayOutputStream baos = new ByteArrayOutputStream();
      PrintStream ps = new PrintStream(baos);
      PrintStream old = System.out;
      System.setOut(ps);
      logger.info(String.format(">>> Order ID: %d, QTY: %d, Notes: %s", orderID.incrementAndGet(), order.getQty(), comments));
      System.out.flush();
      System.setOut(old);
      response = baos.toString().split(">>>", 2)[1];
    } else {
      response = "Haha! Nice try hacker--but my web-application is *super secure*!";
    }

    return response;
  }
}