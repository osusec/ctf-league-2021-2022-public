// Web Controller to return templatized web page
package com.corporate.coffeeCorporation;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.*;

@Controller
public class AppController {
  
  @GetMapping("/")
  public String index(Model model) {
    ArrayList<Coffee> inventory = (new CoffeeController()).available();

    ArrayList<String> coffeeNames = new ArrayList<String>();
    for (int i=0; i < inventory.size(); i++){
      coffeeNames.add(inventory.get(i).getBrand());
    }

    model.addAttribute("order", new OrderForm());
    model.addAttribute("coffee", coffeeNames);
    return "home";
  }

}