package com.corporate.coffeeCorporation;


public class Coffee {

	private final int id;
  private final int stock;
  private final String brand;

	public Coffee(int id, int stock, String brand) {
    this.id = id;
    this.stock =stock;
    this.brand = brand;
	}

  public int getId(){
    return this.id;
  }
  public int getStock(){
    return this.stock;
  }
  public String getBrand(){
    return this.brand;
  }
}