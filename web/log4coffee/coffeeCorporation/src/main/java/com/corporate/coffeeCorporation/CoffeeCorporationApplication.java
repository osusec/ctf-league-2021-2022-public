package com.corporate.coffeeCorporation;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CoffeeCorporationApplication {

	public static void main(String[] args) {
		// Just making sure we're 100% exploitable
    System.setProperty("com.sun.jndi.ldap.object.trustURLCodebase", "true");
		System.setProperty("log4j2.formatMsgNoLookups", "false");
		System.setProperty("log4j2.enableJndiLookup", "true");

		SpringApplication.run(CoffeeCorporationApplication.class, args);
	}

}
