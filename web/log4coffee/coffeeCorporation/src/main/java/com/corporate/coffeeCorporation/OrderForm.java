package com.corporate.coffeeCorporation;

public class OrderForm {
  private int id;
  private int qty;
  private String comments;

  /* Setters */
  public void setId(int id){
    this.id = id;
  }

  public void setQty(int qty){
    this.qty = qty;
  }

  public void setComments(String comments){
    this.comments = comments;
  }

  /* Getters */
  public int getId(){
    return this.id;
  }
  public int getQty(){
    return this.qty;
  }
  public String getComments(){
    return this.comments;
  }
}