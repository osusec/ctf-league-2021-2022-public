# hash-browns

**Title**: hash-browns

**Category**: web

**Points**: 200

**Download Link**: None

**Access**: http://chal.ctf-league.osusec.org:31337/

**Description**: Welcome to the OSUSEC CTF League! The goal of each of these challenges is to find a flag that will be in the form `osu{n0t_a_fL4g}`. When you capture the flag, submit it in this channel using `$submit <flag>`. To start, open the linked website in your browser. Good luck!

**Flag**: osu{p1n34ppl3_h45h_Br0wN5_4r3_g00D}