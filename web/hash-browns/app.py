from flask import Flask, render_template, send_file, request
import hashlib

app = Flask(__name__)

@app.route("/")
def index():
    return send_file("index.html")

@app.route("/login", methods=['POST', 'GET'])
def login():
    password = None
    try:
        password = request.form['password']
    except:
        pass
    if password and hashlib.sha256(password.encode('utf-8')).hexdigest() == "b0fef621727ff82a7d334d9f1f047dc662ed0e27e05aa8fd1aefd19b0fff312c":
        return send_file("flag.html")
    else:
        return "Unauthorized"

@app.route('/static/<path:path>')
def send_static(path):
    return send_from_directory('static', path)
