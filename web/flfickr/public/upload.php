<!-- This was completely stolen from W3 schools lol -->
<!-- https://www.w3schools.com/php/php_file_upload.asp -->
<html>

    <head>
        <title>flfickr</title>
    </head>
    <body style="padding: 0px; margin: 0px;">
        <div style="background-color: #ddd; width: 100%; padding: 5pt;">
            <img src="flfickr.png" style="width: 20%;">
        </div>
        <div style="padding: 10pt;" id="content">

<?php
$target_dir = "uploads/";
$target_file = $target_dir . basename($_FILES["fileToUpload"]["name"]);
$uploadOk = 1;
$imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));

// Check if image file is a actual image or fake image
if(isset($_POST["submit"])) {
  $check = getimagesize($_FILES["fileToUpload"]["tmp_name"]);
  if($check !== false) {
    echo "File is an image - " . $check["mime"] . ".<br>";
    $uploadOk = 1;
  } else {
    echo "File is not an image.<br>";
    $uploadOk = 0;
  }
}

// Check if file already exists
if (file_exists($target_file)) {
  echo "Sorry, file already exists.<br>";
  $uploadOk = 0;
}

// Check file size
if ($_FILES["fileToUpload"]["size"] > 500000) {
  echo "Sorry, your file is too large.<br>";
  $uploadOk = 0;
}

// Allow certain file formats
if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
&& $imageFileType != "gif" ) {
  echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.<br>";
  $uploadOk = 0;
}

// Check if $uploadOk is set to 0 by an error
if ($uploadOk == 0) {
  echo "Sorry, your file was not uploaded.<br>";
// if everything is ok, try to upload file
} else {
  if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)) {
    echo "The file ". htmlspecialchars( basename( $_FILES["fileToUpload"]["name"])). " has been uploaded.<br>";
    $imgPath = "uploads/" . $_FILES["fileToUpload"]["name"];
    echo 'Use this link to share your image! <a id="imgLink" href="' . $imgPath . '">' . $imgPath . "</a>";
  } else {
    echo "Sorry, there was an error uploading your file.<br>";
  }
}
?>

        </div>
        <script>
            let imgLink = document.getElementById("imgLink");
            if (imgLink) {
                console.log(imgLink.innerText)
                imgLink.innerText = new URL(imgLink.innerText, document.baseURI).href;
            }
        </script>
    </body>
</html>