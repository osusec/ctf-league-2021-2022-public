<html>
    <head>
        <title>flfickr</title>
    </head>
    <body style="padding: 0px; margin: 0px;">
        <div style="background-color: #ddd; width: 100%; padding: 5pt; display: flex;">
            <img src="flfickr.png" style="width: 20%;">
            <select name="lang" id="lang" style="margin-left: auto;" onchange="switchLang()">
                <option value="english">English</option>
                <option value="chinese">Chinese</option>
                <option value="german">German</option>
                <option value="swahili">Swahili</option>
            </select>
            <script>
                document.getElementById("lang").value = window.location.href.split('=')[1]
                function switchLang() {
                    window.location = window.location.href.split('?')[0] + '?language=' + document.getElementById("lang").value
                }
            </script>
        </div>
        <div style="padding: 10pt;">
            <?php
                $lang = "english";
                if (isset($_GET["language"])) {
                    $lang = $_GET["language"];
                }
                include("languages/$lang");
            ?>
        </div>
    </body>
</html>
