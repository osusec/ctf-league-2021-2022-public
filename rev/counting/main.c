#include <stdio.h>
#include <stdbool.h>
#include <string.h>

bool check_password(char input[256]);
bool check_char(int i, char input[256]);
unsigned char mangle_char(int i, char input[256]);

char password[] = "\x46\x41\xe5\xb3\xcf\x36\x14\x05\xec\xc6\x40\x7c\xac\x6a\x90\x8e\x6b\xb5\x00\x09\x75\xaf\x34\xad\x84\x70\x8d\xf0";
char key[] = "\x85\x4d\x23\xe4\xbf\xe5\x29\xf2\xef\xa2\xdb\xbf\xb4\x72\xa9\xe1\x5b\x13\x1e\xb5\xed\xca\x04\xbe\xa3\x35\x8f\x0f";

int main() {
    char input[256];

    printf("Enter the password: ");
    fgets(input, 256, stdin);
    *strchr(input, '\n') = '\0';

    printf(check_password(input) ? "Correct!\n" : "Wrong\n");

    return 0;
}

bool check_password(char input[256]) {
    for (int i = 0; i < strlen(input); ++i) {
        if (!check_char(i, input)) return false;
    }
    return strlen(input) == 28;
}

bool check_char(int i, char input[256]) {
    return (unsigned char)password[i] == mangle_char(i, input);
}

unsigned char mangle_char(int i, char input[256]) {
    unsigned char c = input[i];
    int l = strlen(key);
    if (c % 2 == 0) {
        c = (c + 0x42) % 256;
    }
    else {
        c = (c + 0xee) % 256;
    }
    c = (c << (i % 8))|(c >> (8 - (i % 8)));
    c = c ^ key[(i + 3) % l];
    c = (c >> (i % 8))|(c << (8 - (i % 8)));
    c = 255 - c;
    return c;
}