#!/bin/env python3
import random

names = ['azurediamond', 'sergeantGeech', 'testbaboon', 'toxic_Y', 'Aqcurate', 'BaboonWithTheGoon', 'hm3k', 'Lance Roy', 'lyellread', 'm0x', 'perchik', 'Toxic_Z', 'captainGeech', 'WholeWheatBagels']
passwords = ['password', 'asdf', 'supersecure', 'E4Ef6mhfj5', 'OxQ92r8Fq3', '76JMuqr4UD']

users = ''

for n in names:
    score = random.randint(1, 25)
    games_played = random.randint(1, 20)
    password = random.choice(passwords)
    user = f'INSERT INTO users (username, password, high_score, games_played) VALUES ("{n}", "{password}", {score}, {games_played});'
    users += f'{user}\n'

print(users)