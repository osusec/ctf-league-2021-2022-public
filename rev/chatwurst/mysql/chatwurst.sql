DROP DATABASE IF EXISTS chatwurst;
CREATE DATABASE chatwurst;
USE chatwurst;

CREATE TABLE messages (
    message_id INT NOT NULL AUTO_INCREMENT,
    group_id INT NOT NULL,
    user_id INT NOT NULL,
    time_sent TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    content TEXT,
    PRIMARY KEY (message_id)
);

CREATE TABLE users (
    user_id INT NOT NULL AUTO_INCREMENT,
    username VARCHAR(255) NOT NULL,
    password VARCHAR(255) NOT NULL,
    PRIMARY KEY (user_id)
);

CREATE TABLE chat_groups (
    group_id INT NOT NULL AUTO_INCREMENT,
    time_updated TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    user_0 INT,
    user_1 INT,
    user_2 INT,
    user_3 INT,
    user_4 INT,
    user_5 INT,
    user_6 INT,
    user_7 INT,
    user_8 INT,
    user_9 INT,
    PRIMARY KEY (group_id)
);

INSERT INTO users (username, password) VALUES ("balkerntuple", "SuperSecretPassword4!");
INSERT INTO users (username, password) VALUES ("FizzbuzzMcFlurry", "SuperSecretPassword4!");
INSERT INTO chat_groups (user_0, user_1) VALUES (1, 2);
INSERT INTO messages (group_id, user_id, content) VALUES (1, 2, "hey do you want the flag");
INSERT INTO messages (group_id, user_id, content) VALUES (1, 1, "yeah sure");
INSERT INTO messages (group_id, user_id, content) VALUES (1, 2, "ok here is the flag");
INSERT INTO messages (group_id, user_id, content) VALUES (1, 2, "osu{cH4TWUrst_m0RE_L1KE_wUrSt_ch47}");

