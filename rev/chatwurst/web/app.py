#!/usr/bin/env python3

from flask import Flask, request, Response
from DBUtil import *
from config import *
from datetime import datetime, timedelta

app = Flask(__name__)
db = DBUtil(DB_CONFIG)

# Create a user with a specified username and password
# username, password
# returns user_id
@app.route("/create_user", methods=['POST'])
def create_user():
    connection = db.begin_sql_connection()
    username = None
    password = None
    content = request.json
    try:
        username = content["username"]
        password = content["password"]
    except:
        pass
    if username == None or password == None or username == "" or password == "":
        db.end_sql_connection(connection)
        return {"error": "missing username or password"}, 200
    res = sql_fetchone(connection, "SELECT username FROM users WHERE username=%s", val=(username,))
    if res != None:
        db.end_sql_connection(connection)
        return {"error": "user already exists"}, 200
    else:
        sql_commit(connection, "INSERT INTO users (username, password) VALUES (%s, %s)", val=(username, password))
        res = sql_fetchone(connection, "SELECT user_id FROM users WHERE username=%s and password=%s", val=(username, password))
        db.end_sql_connection(connection)
        return {"user_id": res[0]}, 200

# Use several usernames to create a group
# Credential, user_0, user_1, user_2, ...
# returns group_id
@app.route("/create_group", methods=['POST'])
def create_group():
    connection = db.begin_sql_connection()
    username = None
    password = None
    content = request.json
    try:
        credential = content["credential"]
        username = credential["username"]
        password = credential["password"]
    except:
        pass
    if username == None or password == None:
        db.end_sql_connection(connection)
        return {"error": "missing username or password"}, 200

    # auth
    res = sql_fetchone(connection, "SELECT user_id FROM users WHERE username=%s and password=%s", val=(username, password))
    if res != None:
        # add group
        usernames = []
        for i in range(0, 10):
            if f"user_{i}" not in content:
                break
            else:
                usernames += [content[f"user_{i}"]]
        if len(usernames) < 2:
            db.end_sql_connection(connection)
            return {"error": "group must have at least one valid user"}, 200
        sql = " OR ".join(["username = %s"] * len(usernames))
        userids = [x[0] for x in sql_fetchall(connection, f"SELECT user_id FROM users WHERE {sql}", val=tuple(usernames))]
        if len(userids) < 2:
            db.end_sql_connection(connection)
            return {"error": "group must have at least one valid user"}, 200
        print(f'SELECT group_id FROM chat_groups WHERE {" AND ".join(["user_" + str(i) + ("=%s" if i < len(userids) else " IS NULL") for i in range(10)])}' % tuple(userids))
        group_id = sql_fetchone(connection, f'SELECT group_id FROM chat_groups WHERE {" AND ".join(["user_" + str(i) + ("=%s" if i < len(userids) else " IS NULL") for i in range(10)])}', val=tuple(userids))
        if group_id == None:
            sql_commit(connection, f'INSERT INTO chat_groups ({", ".join([f"user_{i}" for i in range(len(userids))])}) VALUES ({", ".join(["%s"]*len(userids))})', val=tuple(userids))
            group_id = sql_fetchone(connection, f'SELECT group_id FROM chat_groups WHERE {" AND ".join(["user_" + str(i) + ("=%s" if i < len(userids) else " IS NULL") for i in range(10)])}', val=tuple(userids))
        db.end_sql_connection(connection)
        return {"group_id": group_id[0]}
    else:
        db.end_sql_connection(connection)
        return {"error": "incorrect username or password"}, 200

# Create a message
# Credential, group_id, content
# returns group_id
@app.route("/create_message", methods=['POST'])
def create_message():
    connection = db.begin_sql_connection()
    username = None
    password = None
    content = request.json
    try:
        credential = content["credential"]
        username = credential["username"]
        password = credential["password"]
    except:
        pass
    if username == None or password == None:
        db.end_sql_connection(connection)
        return {"error": "missing username or password"}, 200

    # auth
    res = sql_fetchone(connection, "SELECT user_id FROM users WHERE username=%s and password=%s", val=(username, password))
    if res != None:
        # add message
        user_id = res[0]
        if "group_id" not in content:
            db.end_sql_connection(connection)
            return {"error": "missing group id"}, 200
        if "content" not in content:
            db.end_sql_connection(connection)
            return {"error": "missing content"}, 200
        try: 
            sql_commit(connection, "INSERT INTO messages (group_id, user_id, content) VALUES (%s, %s, %s)", val=(content["group_id"], user_id, content["content"]))
            sql_commit(connection, "UPDATE chat_groups SET time_updated=CURRENT_TIMESTAMP WHERE group_id=%s", val=(content["group_id"],))
            db.end_sql_connection(connection)
            return {}, 200
        except:
            db.end_sql_connection(connection)
            return {"error": "database unavailable"}

    else:
        db.end_sql_connection(connection)
        return {"error": "incorrect username or password"}, 200

# Get user id of a user
# username, password
# returns user_id
@app.route("/login", methods=['POST'])
def login():
    connection = db.begin_sql_connection()
    username = None
    password = None
    content = request.json
    try:
        username = content["username"]
        password = content["password"]
    except:
        pass
    if username == None or password == None:
        db.end_sql_connection(connection)
        return {"error": "missing username or password"}, 200

    # auth
    res = sql_fetchone(connection, "SELECT user_id FROM users WHERE username=%s and password=%s", val=(username, password))
    if res == None:
        db.end_sql_connection(connection)
        return {"error": "incorrect username or password"}, 200
    db.end_sql_connection(connection)
    return {"user_id": res[0]}

# Get group ids for a user id
# Credential, user id
# returns group_ids
@app.route("/get_groups", methods=['POST'])
def get_groups():
    connection = db.begin_sql_connection()
    username = None
    password = None
    content = request.json
    try:
        credential = content["credential"]
        username = credential["username"]
        password = credential["password"]
    except:
        pass
    if username == None or password == None:
        db.end_sql_connection(connection)
        return {"error": "missing username or password"}, 200

    # auth
    res = sql_fetchone(connection, "SELECT user_id FROM users WHERE username=%s and password=%s", val=(username, password))
    if res != None:
        # get user_id
        if "user_id" not in content:
            db.end_sql_connection(connection)
            return {"error": "missing user id"}, 200
        res = sql_fetchall(connection, f'SELECT group_id, {", ".join(["user_" + str(i) for i in range(10)])} FROM chat_groups WHERE {" OR ".join(["user_" + str(i) + "=%s" for i in range(10)])}', val=tuple([content["user_id"]] * 10))
        groups = []
        if res != None: 
            for g in res:
                # get usernames for users in group
                user_ids = tuple(filter(lambda x: x != None, g[1:]))
                unames_res = sql_fetchall(connection, 'SELECT username FROM users WHERE ' + ' OR '.join(['user_id=%s'] * len(user_ids)), val=user_ids)
                groups += [{"group_id": g[0], "usernames": [x[0] for x in unames_res]}]
        db.end_sql_connection(connection)
        return {"groups": groups}
    else:
        db.end_sql_connection(connection)
        return {"error": "incorrect username or password"}, 200

# Get messages for a group id
# Credential, group id
# returns arr of messages
@app.route("/get_messages", methods=['POST'])
def get_messages():
    connection = db.begin_sql_connection()
    username = None
    password = None
    content = request.json
    try:
        credential = content["credential"]
        username = credential["username"]
        password = credential["password"]
    except:
        pass
    if username == None or password == None:
        db.end_sql_connection(connection)
        return {"error": "missing username or password"}, 200

    # auth
    res = sql_fetchone(connection, "SELECT user_id FROM users WHERE username=%s and password=%s", val=(username, password))
    if res != None:
        # get messages
        if "group_id" not in content:
            db.end_sql_connection(connection)
            return {"error": "missing group id"}, 200
        res = sql_fetchall(connection, f'SELECT username, time_sent, content FROM messages INNER JOIN users ON messages.user_id=users.user_id WHERE group_id=%s ORDER BY time_sent', val=(content["group_id"],))
        messages = []
        if res != None: 
            messages = [{
                "username": x[0],
                "time_sent": x[1],
                "content": x[2]
            } for x in res] 
        db.end_sql_connection(connection)
        return {"messages": messages}
    else:
        db.end_sql_connection(connection)
        return {"error": "incorrect username or password"}, 200

if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0', port=5001)
    #serve(app, host='0.0.0.0', port=5000)
