from pwn import *

# exploit

#p = process("./ultrasecure")
p = remote("0.0.0.0", 4545)
p.recvuntil("less than .05s: ")
nonce = p.recv()
p.sendline(nonce)
print(p.recv())
p.sendline(str(int('0xdeadb33f', 16)))
data = p.recv()
print(data)
p.interactive()
