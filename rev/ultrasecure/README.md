# ultrasecure

**Title**: ultrasecure

**Category**: rev

**Points**: 200

**Download Link**: http://chal.ctf-league.osusec.org:1337/ultrasecure

**Access**: http://chal.ctf-league.osusec.org:4545/

**Description**: Use `pwntools` and `ghidra` to reverse engineer and break into the ultrasecure(tm) vault!

**Flag**: osu{d3c0mp1ler_go_brrrr}
