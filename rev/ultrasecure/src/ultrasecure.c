#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <time.h>

void print_flag(){
    FILE *fp;
    char flag[0x40] = {0};
    fp = fopen("./flag", "r");
    fgets(flag, 0x40, fp);
    puts(flag);
    fclose(fp);
}

void password_check(){
    struct timeval start_time, end_time;

    int password = 0xdeadb33f;
    int user_try = 0;
    int nonce = 0;
    int user_nonce = 0;

    srand(time(NULL));
    nonce = rand();


    //nonce check
    gettimeofday(&start_time, NULL);
    printf("Prove that you are not human, repeat this to me in less than .05s: %d\n", nonce);
    scanf("%d", &user_nonce);
    gettimeofday(&end_time, NULL);

    long time_difference = (end_time.tv_sec - start_time.tv_sec) * 1000000 + end_time.tv_usec - start_time.tv_usec;

    if (time_difference > 50000){
        puts("Whoops, too slow");
        exit(-96);
    }

    if (user_nonce != nonce){
        puts("Whoops, wrong nonce!");
        exit(-42);
    }

    //password check
    puts("You passed the nonce check! Now, Unlock the UltraSecure(tm) Vault:");
    scanf("%d", &user_try);

    if (user_try == password){
        print_flag();
        exit(0);
    }
    else{
        puts("Whoops, wrong password :(");
        exit(-69);
    }
}


int main(){
    setbuf(stdin, NULL);
    setbuf(stdout, NULL);
    password_check();
}
