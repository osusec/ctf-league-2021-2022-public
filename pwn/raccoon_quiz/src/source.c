#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include<fcntl.h>

void super_sneaky_function(){
    puts("Wow! You sure are one sneaky raccoon! You can have the flag.");
    char flag[100];
    int fd = open("./flag", O_RDONLY);
    read(fd, flag, 100);
    printf("%s\n", flag);

    _exit(1);
}

void raccoon_quiz(){
    char answer;
    char name[512];
    int num_correct = 0;

    printf("Hewwo! Welcome to the GREAT RACCOON QUIZ :3\n");

    printf("\nQuestion 1: Which president had a raccoon as their pet?\n");
    printf("A: Coolidge\nB: Garfield\nC: Mckinley\nD: Roosevelt\n");
    printf("Answer: ");
    scanf(" %c", &answer);

    if(answer == 'A'){
        printf("Correct!\n\n");
        num_correct++;
    }else{
        printf("Incorrect!\n\n");
    }

    printf("Question 2: What is the average lifespan of a wild raccoon?\n");
    printf("A: 1 - 2 years\nB: 2 - 3 years\nC: 3 - 4 years\nD: 4 - 5 years\n");
    printf("Answer: ");
    scanf(" %c", &answer);

    if(answer == 'B'){
        printf("Correct!\n\n");
        num_correct++;
    }else{
        printf("Incorrect!\n\n");
    }

    printf("Question 3: Do raccoons like hotdogs?\n");
    printf("A: Yes\nB: No\n");
    printf("Answer: ");
    scanf(" %c", &answer);

    if(answer == 'A'){
        printf("Correct! Here is proof: https://youtu.be/Ofp26_oc4CA\n\n");
        num_correct++;
    }else{
        printf("Incorrect!\n\n");
    }

    if(num_correct == 3){
        printf("Congwats! You scored a 3/3. Please enter your name for the leaderboards!\n");
        scanf("%*[^\n]"); 
        scanf("%*c");

        fgets(name, 512, stdin);
        printf("Good job ");
        printf(name);
    }else{
        printf("Uh oh. Looks like you didn't get a perfect score :( Feel free to try again.\n");
    }

    printf("\nGoodbye!\n");

    exit(1);
}

int main(){
    raccoon_quiz();
}
