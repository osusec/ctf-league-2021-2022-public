#include <stdio.h>
#include <stdlib.h>

void print_flag(){
    FILE *fp;
    char flag[0x40] = {0};
    fp = fopen("./flag", "r");
    fgets(flag, 0x40, fp);
    puts(flag);
    fclose(fp);
}

void user_input(){
    puts("Please Enter Text");
    char input[16] = {0};
    fgets(input, 200, stdin);
}


int main(){
    setbuf(stdin, NULL);
    setbuf(stdout, NULL);
    user_input();
    puts("Try again! Control Flow Returned to main()");
}
