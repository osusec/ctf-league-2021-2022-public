#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include <fcntl.h>
#include <sys/wait.h>

void child(){
    char buf[200];
    printf("Please give me some data, I'll store it at %p.\n", buf);
    char chr;
    char * buf_end = buf;

    for (int i = 0; i <= 0x200; i++){
        chr = getchar();
        if ( chr == '\n'){
            break;
        }
        *buf_end=chr;
        buf_end++;
    }
    // printf("%s", buf);
}

int main(){
    int pid = -1;
    int ret;
    int status;

    printf("Welcome to Data Storage as a Services (DSaaS).\n");

    while(1){
        pid = fork();
        if(pid == 0){
            child();
            return EXIT_SUCCESS;
        }
        // wait(NULL);

        int status;
        if ( waitpid(pid, &status, 0) == -1 ) {
            perror("waitpid failed");
            return EXIT_FAILURE;
        }

        if ( WIFEXITED(status) ) {
            const int es = WEXITSTATUS(status);
            printf("exit status was %d\n", es);
        }
    }
}
