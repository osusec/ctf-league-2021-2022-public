#!/bin/sh

# no stderr
exec 2>/dev/null

# dir
cd /chal

timeout -k1 500 stdbuf -i0 -o0 -e0 ./cookies-with-a-fork 2>&1
