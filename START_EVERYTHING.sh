# EVERYTHING KEEPS CRASHING AAAAAAAAAAAAAAAAAAAAAAAAAA
# Start 3 tmux sessions, one for marvin, one for the scoreboard, and one for general challenge stuff
cd ~/ctf_bot
tmux new -s bot -d
tmux send -t bot.0 python3 SPACE bot.py ENTER
tmux split-window -t bot.0
tmux send -t bot.1 sudo SPACE mysql SPACE -u SPACE root SPACE -p SPACE ctf_league ENTER ENTER
tmux send -t bot.1 ENTER

cd ~/ctf_league_scoreboard
tmux new -s scoreboard -d
tmux send -t scoreboard.0 flask SPACE run SPACE --host=0.0.0.0 SPACE --port=1337 ENTER

cd ~/ctf-league-2021-2022
tmux new -s challenges -d

# ############### START CHALLENGES ###################
tmux send -t challenges.0 sudo SPACE docker SPACE run SPACE -p SPACE 31337:5000 SPACE -d SPACE hash-browns ENTER
tmux send -t challenges.0 sudo SPACE docker SPACE run SPACE -p SPACE 4545:4545 SPACE -d SPACE ultrasecure ENTER
tmux send -t challenges.0 sudo SPACE docker SPACE run SPACE -p SPACE 9090:9090 SPACE -d SPACE shellcoding11 ENTER
tmux send -t challenges.0 sudo SPACE docker SPACE run SPACE -p SPACE 4646:4646 SPACE -d SPACE secure-encryptor ENTER
tmux send -t challenges.0 sudo SPACE docker SPACE run SPACE -p SPACE 4747:4545 SPACE -d SPACE babypwn ENTER
tmux send -t challenges.0 sudo SPACE docker SPACE run SPACE -p SPACE 9999:9999 SPACE -d SPACE aaron-debugger ENTER
tmux send -t challenges.0 sudo SPACE docker SPACE run SPACE -p SPACE 6969:6969 SPACE -d SPACE deprecated_echo ENTER

tmux send -t challenges.0 cd SPACE ~/ctf-league-2021-2022/rev/chatwurst ENTER
tmux send -t challenges.0 sudo SPACE docker-compose SPACE d own ENTER
tmux send -t challenges.0 sudo SPACE docker-compose SPACE u p SPACE -d ENTER

tmux send -t challenges.0 cd SPACE ~/ctf-league-2021-2022/misc/marvin ENTER
tmux send -t challenges.0 sudo SPACE docker-compose SPACE d own ENTER
tmux send -t challenges.0 sudo SPACE docker-compose SPACE u p SPACE -d ENTER

tmux send -t challenges.0 sudo SPACE docker SPACE run SPACE -p SPACE 4816:4816 SPACE -d SPACE raccoon_quiz ENTER
tmux send -t challenges.0 sudo SPACE docker SPACE run SPACE -p SPACE 5128:5128 SPACE -d SPACE really_secure_algorithm ENTER

tmux send -t challenges.0 sudo SPACE docker SPACE run SPACE -p SPACE 8080:5149 SPACE -d SPACE log4coffee ENTER
tmux send -t challenges.0 sudo SPACE docker SPACE run SPACE -p SPACE 8888:8888 SPACE -p SPACE 1389:1389 SPACE -d SPACE log4jexp ENTER

tmux send -t challenges.0 sudo SPACE docker SPACE run SPACE -p SPACE 8231:8231 SPACE -d SPACE jailed ENTER

tmux send -t challenges.0 sudo SPACE docker SPACE run SPACE -p SPACE 7475:80 SPACE -d SPACE flfickr ENTER

tmux send -t challenges.0 cd SPACE ~/ctf-league-2021-2022 ENTER

echo "Make sure to check that this stuff is all good"
sleep 5
sudo docker ps

