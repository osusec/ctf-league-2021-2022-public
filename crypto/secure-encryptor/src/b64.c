#include "b64.h"

size_t encode_b64(const char* input, char* output, int input_length) {
    base64_encodestate s;
    size_t cnt;

    base64_init_encodestate(&s);
    cnt = base64_encode_block(input, input_length, output, &s);
    cnt += base64_encode_blockend(output + cnt, &s);
    output[cnt] = 0;

    return cnt;
}

size_t decode_b64(const char* input, char* output) {
    base64_decodestate s;
    size_t cnt;

    base64_init_decodestate(&s);
    cnt = base64_decode_block(input, strlen(input), output, &s);
    output[cnt] = 0;

    return cnt;
}
