#include <b64/cencode.h>
#include <b64/cdecode.h>
#include <string.h>

size_t encode_b64(const char*, char*, int);
size_t decode_b64(const char* input, char* output);