#define _GNU_SOURCE

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <time.h>
#include "b64.h"

// OTP length
#define OTP_LENGTH 1024

// Max space used when encoded in base 64, can be calculated with ceil(OTP_LENGTH/3)*4
#define MAX_B64 1400


//
// Returns pointer to a OTP of length "OTP_LENGTH"
//
char *get_otp(void) {
	
	char *otp = calloc(OTP_LENGTH, sizeof(char));

	FILE *f = fopen("/dev/urandom", "r");
	fread(otp, sizeof(char), OTP_LENGTH, f);
	fclose(f);

	return otp;
}


//
// Read flag from ./flag and randomly place it somewhere in a string of length "OTP_LENGTH"
//
char *get_flag(void) {

	char *flag = NULL;
	size_t flag_len = 0;

	FILE *f = fopen("./flag", "r");
	getline(&flag, &flag_len, f);
	flag[strlen(flag) - 1] = '\0';
	fclose(f);

	return flag;
}


//
// Pad the plaintext flag to be the length of the OTP and place it randomly in the string.
//
char *random_pad(char *plaintext) {

	// Generate lengths for pre and post pads
	int plaintext_len = strlen(plaintext);
	int pre_plaintext_len = rand() % (OTP_LENGTH - plaintext_len - 1);
	int post_plaintext_len = OTP_LENGTH - pre_plaintext_len - plaintext_len;


	// Build plaintext that is same length as OTP
	char *padded_flag = calloc(OTP_LENGTH+1, sizeof(char));

	FILE *f = fopen("/dev/urandom", "r");
	fread(padded_flag, sizeof(char), pre_plaintext_len, f);
	memcpy(padded_flag + pre_plaintext_len, plaintext, plaintext_len);
	fread(padded_flag + pre_plaintext_len + plaintext_len, sizeof(char), post_plaintext_len, f);
	fclose(f);

	return padded_flag;
}


//
// Encrypt the plaintext very securely
//
char *encrypt(char *plaintext, char *otp) {

	char ciphertext[OTP_LENGTH];
	memcpy(ciphertext, plaintext, OTP_LENGTH);
	memfrob(ciphertext, OTP_LENGTH);
	for (int i = 0; i < OTP_LENGTH; i++) {
		ciphertext[i] = ciphertext[i] ^ otp[i];
	}

	char *encoded_ciphertext = calloc(MAX_B64, sizeof(char));
	encode_b64(ciphertext, encoded_ciphertext, OTP_LENGTH);

	return encoded_ciphertext;
}


int main(void) {
	
	setbuf(stdin, NULL);
	setbuf(stdout, NULL);
	srand(time(NULL));

	char *otp = get_otp();
	char *flag = get_flag();
	char *padded_flag = random_pad(flag);
	char *ciphertext = encrypt(padded_flag, otp);
	
	printf("Here's the flag, I've just hidden it a little bit...\n%s\n", ciphertext);
	printf("I will encrypt exactly one plaintext for you: ");
	
	char user_input[OTP_LENGTH+1];
	scanf("%1024s", user_input);

	char *padded_input = random_pad(user_input);
	char *encrypted_input = encrypt(padded_input, otp);

	printf("Here is your ciphertext, hopefully you can find the flag!\n%s\n", encrypted_input);

	return 0;
}
